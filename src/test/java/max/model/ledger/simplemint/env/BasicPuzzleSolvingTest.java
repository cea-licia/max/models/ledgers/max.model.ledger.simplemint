/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplemint.env;

import max.core.action.ACTakeRole;
import max.core.role.RExperimenter;
import max.model.ledger.abstract_ledger.action.check.AcCheckCoherence;
import max.model.ledger.abstract_ledger.action.check.AcCollectDeliveredTransactionsMetrics;
import max.model.ledger.abstract_ledger.usecase.puzzle.action.AcBroadcastClientPuzzleSolutions;
import max.model.ledger.abstract_ledger.usecase.puzzle.action.AcCheckOrderFairnessForPuzzles;

import max.model.ledger.simplemint.behavior.TmProposerSelector;
import max.model.ledger.simplemint.exp.TendermintAgent;
import max.model.ledger.simplemint.exp.TendermintExperimenter;
import max.model.ledger.simplemint.role.RTendermintChannel;
import max.model.ledger.simplemint.role.RTendermintValidator;
import max.model.network.stochastic_adversarial_p2p.action.AcCollectMessageCount;
import max.model.network.stochastic_adversarial_p2p.context.delay.ContinuousUniformDelay;
import max.model.network.stochastic_adversarial_p2p.context.delay.DelaySpecification;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static max.core.MAXParameters.*;
import static max.core.test.TestMain.launchTester;

/**
 * Basic Test in which we set up a Tendermint network
 * and in which clients periodically broadcast puzzle solutions to all the Tendermint validators.
 *
 * @author Erwan Mahe
 */
public class BasicPuzzleSolvingTest {

    @BeforeEach
    public void before(@TempDir Path tempDir) {
        clearParameters();

        setParameter("MAX_CORE_SIMULATION_STEP", BigDecimal.ONE.toString());
        setParameter("MAX_CORE_UI_MODE", "Silent");
        setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
    }


    @ParameterizedTest
    @CsvSource({"3,5"})
    public void test(int byzNum, int clientCount, TestInfo testInfo)
            throws Throwable {

        boolean printLogs = false;

        int nodeCount = 3 * byzNum + 1;

        List<String> clientsList = new ArrayList<>();
        for (int i=0;i< clientCount;i++) {
            clientsList.add("client" + String.valueOf(i));
        }


        // Tendermint phase timeout
        BigDecimal tendermintPhaseTimeoutDuration = BigDecimal.valueOf(25);

        // rate of new puzzles
        BigDecimal new_puzzles_rate = BigDecimal.valueOf(10);

        // duration for solving a puzzle
        Optional<DelaySpecification> delayBetweenClientsAndValidators = Optional.of(
                new ContinuousUniformDelay(1, 8)
        );

        Optional<DelaySpecification> baseline_output_delay = Optional.of(
                new ContinuousUniformDelay(5, 10)
        );
        Optional<DelaySpecification> baseline_input_delay = Optional.of(
                new ContinuousUniformDelay(5, 10)
        );

        // Test duration in ticks
        final var testDuration = 1000;

        // Setup tester
        final var tester =
                new TendermintExperimenter() {

                    @Override
                    protected void setupTmNodes() {
                        for (var i = 0;
                             i < nodeCount;
                             ++i) {
                            String tmNodeName = "tmNode" + String.valueOf(i);
                            TendermintAgent tmAgent = new TendermintAgent(
                                    build_tendermint_node_plan(
                                            baseline_output_delay,
                                            baseline_input_delay,
                                            byzNum,
                                            tendermintPhaseTimeoutDuration,
                                            tmNodeName,
                                            printLogs)
                            );
                            tmAgent.setName(tmNodeName);
                            this.addTmNode(tmAgent);
                        }
                        TmProposerSelector.setInstance(this.getTmNodesNames());
                    }


                    @Override
                    protected void setupExperimenter() {
                        schedule(
                                new ACTakeRole<>(this.tmEnvironment.getName(),
                                        RExperimenter.class,
                                        this.tmEnvironment)
                                        .oneTime(BigDecimal.ZERO)
                        );
                        schedule(
                                new ACTakeRole<>(this.tmEnvironment.getName(),
                                        RTendermintChannel.class,
                                        this.tmEnvironment)
                                        .oneTime(BigDecimal.ZERO)
                        );

                        schedule(
                                new AcBroadcastClientPuzzleSolutions(
                                        this.tmEnvironment.getName(),
                                        this.tmEnvironment,
                                        RTendermintValidator.class,
                                        clientsList,delayBetweenClientsAndValidators,
                                        new HashMap<>(),
                                        Optional.empty(),
                                        Optional.empty())
                                        .repeatFinitely(BigDecimal.valueOf(3),BigDecimal.valueOf(testDuration - 3),new_puzzles_rate)
                        );

                        //String csvFilePath = "C:\\Users\\em244186\\idea_projects\\simplemint\\" + "puzzle_" + String.valueOf(nodeCount) + "nodes_" + String.valueOf(clientCount) + "clients.csv";
                        //String prefix = String.join(";", clientsList) + ";rec_ord_fair;blo_ord_fair;dif_ord_fair\n";
                        //String filePath = "C:\\Users\\em244186\\idea_projects\\simplemint\\test.txt";

                        schedule(
                                new AcCheckCoherence<>(
                                        this.tmEnvironment.getName(),
                                        this.tmEnvironment,
                                        RTendermintValidator.class)
                                        .oneTime(testDuration)
                        );
                        schedule(
                                new AcCollectDeliveredTransactionsMetrics<>(
                                        this.tmEnvironment.getName(),
                                        this.tmEnvironment,
                                        RTendermintValidator.class,
                                        Optional.empty())
                                        .oneTime(testDuration)
                        );
                        schedule(
                                new AcCheckOrderFairnessForPuzzles(
                                        this.tmEnvironment.getName(),
                                        this.tmEnvironment,
                                        RTendermintValidator.class,
                                        nodeCount / 2,
                                        2 * byzNum,
                                        clientsList,
                                        clientCount,
                                        Optional.empty())
                                        .oneTime(testDuration)
                        );
                        schedule(
                                new AcCollectMessageCount(
                                        this.tmEnvironment.getName(),
                                        this.tmEnvironment,
                                        Optional.empty())
                                        .oneTime(testDuration)
                        );
                    }
                };
        launchTester(tester, testDuration, testInfo);
    }
}
