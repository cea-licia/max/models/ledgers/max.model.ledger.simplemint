/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplemint.exp;

import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.scheduling.ActionActivator;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupApprovalPolicy;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupLocalLedgerState;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;
import max.model.ledger.abstract_ledger.action.config.AcSetLocalApprovalPolicy;
import max.model.ledger.abstract_ledger.action.config.AcSetLocalLedgerState;
import max.model.ledger.simplemint.action.AcInitializeTendermintContext;

import max.model.ledger.simplemint.behavior.TendermintPhase;
import max.model.ledger.simplemint.action.AcStartNewTendermintHeight;
import max.model.ledger.simplemint.data_types.hasher.TmHasherSingleton;
import max.model.ledger.simplemint.data_types.message.BasicTendermintMessageLogWriter;
import max.model.ledger.simplemint.env.TendermintEnvironment;
import max.model.ledger.simplemint.handler.TmPrecommitMessageHandler;
import max.model.ledger.simplemint.handler.TmPrevoteMessageHandler;
import max.model.ledger.simplemint.handler.TmProposeMessageHandler;
import max.model.ledger.simplemint.mockup.MockupTransactionHasher;
import max.model.ledger.simplemint.role.RTendermintValidator;
import max.model.network.stochastic_adversarial_p2p.action.config.AcAddCommunicationDelay;
import max.model.network.stochastic_adversarial_p2p.action.config.AcConfigMessageHandler;
import max.model.network.stochastic_adversarial_p2p.action.config.AcConfigMessageLogger;
import max.model.network.stochastic_adversarial_p2p.context.delay.DelaySpecification;
import max.model.network.stochastic_adversarial_p2p.context.filter.UniformLevelFilter;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Base experimenter for our tests.
 *
 * @author Erwan Mahe
 */
public abstract class TendermintExperimenter extends ExperimenterAgent {


    /** Tendermint environment. */
    protected TendermintEnvironment<PuzzleMockupTransaction, PuzzleMockupLocalLedgerState,TendermintAgent> tmEnvironment;


    /** Tendermint nodes. */
    private final Map<String, TendermintAgent> tmNodes;

    /**
     * Build a new experimenter instance.
     */
    public TendermintExperimenter() {
        this.tmNodes = new HashMap<>();
    }


    /**
     * Sets up the environment.
     */
    protected void setupEnvironment() {
        // sets up the environment
        this.tmEnvironment = new TendermintEnvironment<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState,TendermintAgent>();
        // sets up the hasher for the transactions and vertices
        TmHasherSingleton.getInstance().initialize_concrete_hasher_for_TM_environment(this.tmEnvironment.getName(),new MockupTransactionHasher());
    }

    /**
     * Adds a Tendermint node to the simulation.
     */
    protected void addTmNode(TendermintAgent node) {
        this.tmNodes.put(node.getName(), node);
    }


    public List<String> getTmNodesNames() {
        return this.tmNodes.keySet().stream().sorted().collect(Collectors.toList());
    }

    /**
     * Sets up the nodes in the simulation.
     * Implementation details are left to the concrete simulation testcase.
     */
    protected abstract void setupTmNodes();


    @Override
    protected List<MAXAgent> setupScenario() {
        this.setupEnvironment();
        this.setupTmNodes();
        return Stream.concat(
                        Stream.of(this.tmEnvironment),
                        this.tmNodes.values().stream()
                )
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /**
     * Build a plan for Tendermint Nodes
     */
    protected Plan<TendermintAgent> build_tendermint_node_plan(
            Optional<DelaySpecification> baseline_output_delay,
            Optional<DelaySpecification> baseline_input_delay,
            int byzantineThresholdF,
            BigDecimal timeoutDuration,
            String name,
            boolean printLogs
    ) {
        // Static part
        final List<ActionActivator<TendermintAgent>> static_plan = new ArrayList<>();
        final var join_commitee_time = getCurrentTick();
        final var setup_time = join_commitee_time.add(BigDecimal.valueOf(1));
        final var start_time = setup_time.add(BigDecimal.valueOf(1));

        // Have the node join the Tendermint commitee
        static_plan.add(new ACTakeRole<TendermintAgent>(this.tmEnvironment.getName(), RTendermintValidator.class,null).oneTime(join_commitee_time));

        // Node setup configuration
        // Setting up the message handlers
        static_plan.add(
                new AcConfigMessageHandler<TendermintAgent>(
                        this.tmEnvironment.getName(),
                        null,
                        TendermintPhase.PROPOSE.name(),
                        new TmProposeMessageHandler<>()
                ).oneTime(setup_time)
        );
        static_plan.add(
                new AcConfigMessageHandler<TendermintAgent>(
                        this.tmEnvironment.getName(),
                        null,
                        TendermintPhase.PREVOTE.name(),
                        new TmPrevoteMessageHandler<>()
                ).oneTime(setup_time)
        );
        if (printLogs) {
            static_plan.add(
                    new AcConfigMessageLogger<TendermintAgent>(
                            this.tmEnvironment.getName(),
                            null,
                            "C:\\Users\\em244186\\idea_projects\\simplemint\\",
                            new BasicTendermintMessageLogWriter()
                    ).oneTime(setup_time)
            );
        }
        static_plan.add(
                new AcConfigMessageHandler<TendermintAgent>(
                        this.tmEnvironment.getName(),
                        null,
                        TendermintPhase.PRECOMMIT.name(),
                        new TmPrecommitMessageHandler<>()
                ).oneTime(setup_time)
        );
        // Setting up the delays
        baseline_output_delay.ifPresent(delay -> {
            AcAddCommunicationDelay<TendermintAgent> add_delay_act = new AcAddCommunicationDelay<TendermintAgent>(this.tmEnvironment.getName(),
                    null,
                    "OUTPUT",
                    "BASELINE",
                    new UniformLevelFilter(1.0),
                    delay);
            static_plan.add(add_delay_act.oneTime(setup_time));
        });
        baseline_input_delay.ifPresent(delay -> {
            AcAddCommunicationDelay<TendermintAgent> add_delay_act = new AcAddCommunicationDelay<TendermintAgent>(this.tmEnvironment.getName(),
                    null,
                    "INPUT",
                    "BASELINE",
                    new UniformLevelFilter(1.0),
                    delay);
            static_plan.add(add_delay_act.oneTime(setup_time));
        });
        // Setting the local ledger state
        static_plan.add(
                new AcSetLocalLedgerState<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState,TendermintAgent>(
                        this.tmEnvironment.getName(),
                        null,
                        new PuzzleMockupLocalLedgerState()
                ).oneTime(setup_time)
        );
        // Initializes the other context attributed
        static_plan.add(
                new AcInitializeTendermintContext<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState,TendermintAgent>(
                        this.tmEnvironment.getName(),
                        null,
                        byzantineThresholdF,
                        timeoutDuration
                ).oneTime(setup_time)
        );

        // Setting the transactions approval policy
        static_plan.add(
                new AcSetLocalApprovalPolicy<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState,TendermintAgent>(
                        this.tmEnvironment.getName(),
                        null,
                        new PuzzleMockupApprovalPolicy(new HashSet<>())
                ).oneTime(setup_time)
        );
        // To start the first height
        static_plan.add(
                new AcStartNewTendermintHeight<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState,TendermintAgent>(
                        this.tmEnvironment.getName(),
                        null).oneTime(start_time)
        );

        return new Plan<TendermintAgent>() {
            @Override
            public List<ActionActivator<TendermintAgent>> getInitialPlan() {
                return static_plan;
            }
        };

    }




}