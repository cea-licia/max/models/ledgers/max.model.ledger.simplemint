/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplemint.action.timeout;


import max.core.action.Action;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.simplemint.action.AcStartNewTendermintRound;
import max.model.ledger.simplemint.action.broadcast.AcBroadcastTendermintVote;

import max.model.ledger.simplemint.behavior.TendermintPhase;
import max.model.ledger.simplemint.env.TendermintContext;
import max.model.ledger.simplemint.role.RTendermintValidator;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;

import java.util.*;


/**
 * Action executed when a Tendermint timeout expires without being cancelled.
 *
 * @author Erwan Mahe
 */
public class AcTendermintTimeout<T_tx, T_st extends AbstractLocalLedgerState<T_tx>, A extends StochasticPeerAgent> extends Action<A> {

    public final TendermintPhase timeoutKind;

    public final int height;

    public final int round;


    public AcTendermintTimeout(String drEnvironmentName,
                               A owner,
                               TendermintPhase timeoutKind,
                               int height,
                               int round) {
        super(drEnvironmentName, RTendermintValidator.class, owner);
        this.timeoutKind = timeoutKind;
        this.height = height;
        this.round = round;
    }

    @Override
    public void execute() {
        TendermintContext<T_tx,T_st,A> context = (TendermintContext<T_tx,T_st,A>) this.getOwner().getContext(this.getEnvironment());

        if (context.currentHeight.equals(this.height) && context.currentRound.equals(this.round)) {
            switch (this.timeoutKind) {
                case PROPOSE:
                    if (context.currentPhase.equals(TendermintPhase.PROPOSE)) {
                        getOwner().getLogger().info("TIMEOUT PROPOSE triggered for validator " + getOwner().getName() + " for height " + this.height + " and round " + this.round);
                        (new AcBroadcastTendermintVote<>(
                                getEnvironment(),
                                getOwner(),
                                this.height,this.round,TendermintPhase.PREVOTE,
                                Optional.empty())
                        ).execute();
                        context.currentPhase = TendermintPhase.PREVOTE;
                        // STORES the prevote in local storage
                        context.localTendermintVotesCounter.storeVote(this.height,this.round,TendermintPhase.PREVOTE,this.getOwner().getName(),Optional.empty());
                    }
                    break;
                case PREVOTE:
                    if (context.currentPhase.equals(TendermintPhase.PREVOTE)) {
                        getOwner().getLogger().info("TIMEOUT PREVOTE triggered for validator " + getOwner().getName() + " for height " + this.height + " and round " + this.round);
                        (new AcBroadcastTendermintVote<>(
                                getEnvironment(),
                                getOwner(),
                                this.height,this.round,TendermintPhase.PRECOMMIT,
                                Optional.empty())
                        ).execute();
                        context.currentPhase = TendermintPhase.PRECOMMIT;
                        // STORES the prevote in local storage
                        context.localTendermintVotesCounter.storeVote(this.height,this.round,TendermintPhase.PRECOMMIT,this.getOwner().getName(),Optional.empty());
                    }
                    break;
                case PRECOMMIT:
                    getOwner().getLogger().info("TIMEOUT PRECOMMIT triggered for validator " + getOwner().getName() + " for height " + this.height + " and round " + this.round);
                    (new AcStartNewTendermintRound<>(getEnvironment(),getOwner(), context.currentRound + 1)).execute();
                    break;
            }
        }
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcTendermintTimeout(
                getEnvironment(),
                getOwner(),
                this.timeoutKind,
                this.height,
                this.round);
    }
}
