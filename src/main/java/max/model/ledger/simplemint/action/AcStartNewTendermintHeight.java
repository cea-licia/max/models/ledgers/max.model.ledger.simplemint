/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplemint.action;


import max.core.action.Action;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;

import max.model.ledger.simplemint.env.TendermintContext;
import max.model.ledger.simplemint.role.RTendermintValidator;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;

import java.util.Optional;


/**
 * Action executed when a Tendermint validator, having just delivered a new block, start the next consensus height.
 *
 * @author Erwan Mahe
 */
public class AcStartNewTendermintHeight<T_tx, T_st extends AbstractLocalLedgerState<T_tx>, A extends StochasticPeerAgent> extends Action<A> {

    public AcStartNewTendermintHeight(String environmentName,
                                      A owner) {
        super(environmentName, RTendermintValidator.class, owner);
    }

    @Override
    public void execute() {
        TendermintContext<T_tx,T_st, A> context = (TendermintContext<T_tx,T_st, A>) this.getOwner().getContext(this.getEnvironment());
        // ***
        // presupposes initialized context
        context.currentHeight = context.currentHeight + 1;
        context.currentRound = -1;
        context.currentPhase = null;
        context.lockedRoundAndValue = Optional.empty();
        context.validRoundAndValue = Optional.empty();
        // retrieve sender information
        String agentName = context.getMyAddress(RTendermintValidator.class.getName()).getAgent().getName();
        // ***
        getOwner().getLogger().info(
                "Node " + agentName +
                        " starting new height : " +
                        String.valueOf(context.currentHeight)
        );
        // ***
        (new AcStartNewTendermintRound<>(this.getEnvironment(),this.getOwner(), 0)).execute();
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcStartNewTendermintHeight(
                getEnvironment(),
                getOwner());
    }
}
