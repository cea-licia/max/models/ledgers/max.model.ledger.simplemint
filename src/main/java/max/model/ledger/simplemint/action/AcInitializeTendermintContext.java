/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplemint.action;


import max.core.action.Action;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;

import max.model.ledger.simplemint.env.TendermintContext;
import max.model.ledger.simplemint.role.RTendermintValidator;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;

import java.math.BigDecimal;
import java.util.Optional;


/**
 * Action executed when a Tendermint validator is initialized.
 *
 * @author Erwan Mahe
 */
public class AcInitializeTendermintContext<T_tx, T_st extends AbstractLocalLedgerState<T_tx>, A extends StochasticPeerAgent> extends Action<A> {

    private final int byzantineThresholdF;

    private final BigDecimal timeoutDuration;

    public AcInitializeTendermintContext(String environmentName,
                                         A owner,
                                         int byzantineThresholdF,
                                         BigDecimal timeoutDuration) {
        super(environmentName, RTendermintValidator.class, owner);
        this.byzantineThresholdF = byzantineThresholdF;
        this.timeoutDuration = timeoutDuration;
    }

    @Override
    public void execute() {
        TendermintContext<T_tx,T_st, A> context = (TendermintContext<T_tx,T_st, A>) this.getOwner().getContext(this.getEnvironment());
        this.getOwner().getLogger().info("initializing Tendermint context with a byzantine threshold of " + this.byzantineThresholdF + " and a timeout duration of " + this.timeoutDuration);
        context.initializeTendermintContext(this.byzantineThresholdF, this.timeoutDuration);
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcInitializeTendermintContext(
                getEnvironment(),
                getOwner(),
                this.byzantineThresholdF,
                this.timeoutDuration);
    }
}
