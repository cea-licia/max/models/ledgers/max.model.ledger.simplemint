/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplemint.action;

import max.core.action.Action;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.simplemint.action.broadcast.AcBroadcastTendermintProposal;
import max.model.ledger.simplemint.action.broadcast.AcBroadcastTendermintVote;

import max.model.ledger.simplemint.behavior.TendermintPhase;
import max.model.ledger.simplemint.behavior.TmProposerSelector;
import max.model.ledger.simplemint.data_types.hasher.TmAbstractTransactionsHasher;
import max.model.ledger.simplemint.data_types.hasher.TmHasherSingleton;
import max.model.ledger.simplemint.env.TendermintContext;
import max.model.ledger.simplemint.role.RTendermintValidator;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Optional;


/**
 * Action executed when a Tendermint validator start a new round of voting.
 *
 * @author Erwan Mahe
 */
public class AcStartNewTendermintRound<T_tx, T_st extends AbstractLocalLedgerState<T_tx>, A extends StochasticPeerAgent> extends Action<A> {


    private final Integer roundToStart;

    public AcStartNewTendermintRound(String environmentName,
                                     A owner,
                                     Integer roundToStart) {
        super(environmentName, RTendermintValidator.class, owner);
        this.roundToStart = roundToStart;
    }

    @Override
    public void execute() {
        TendermintContext<T_tx,T_st,A> context = (TendermintContext<T_tx,T_st,A>) this.getOwner().getContext(this.getEnvironment());
        // ***
        assert(context.currentRound < this.roundToStart) : "the new round to start must be higher than the current round";
        context.currentRound = this.roundToStart;
        context.currentPhase = TendermintPhase.PROPOSE;
        // retrieve sender information
        String agentName = context.getMyAddress(RTendermintValidator.class.getName()).getAgent().getName();
        // ***
        getOwner().getLogger().info(
                "Node " + agentName +
                        " starting new round : " +
                        String.valueOf(context.currentRound)
        );
        // ***
        String proposerName = TmProposerSelector.getInstance().getProposerAtHeightAndRound(context.currentHeight,context.currentRound);
        if (proposerName.equals(agentName)) {
            getOwner().getLogger().info(
                    "Node " + agentName +
                            " is proposer for round : " +
                            String.valueOf(context.currentRound)
            );
            // PRODUCES the proposal
            List<T_tx> proposal;
            Optional<Integer> validRound;
            if (context.validRoundAndValue.isEmpty()) {
                proposal = context.clientTransactionsMempool.extractTransactionsForProposal(
                        Optional.empty(),
                        Optional.empty(),
                        context.approvalPolicy,
                        context.ledgerValidatorState);
                validRound = Optional.empty();
                getOwner().getLogger().info(
                        "Proposing an entirely new proposal with " +
                                proposal.size() + " transactions"
                );
            } else {
                Pair<Integer,List<T_tx>> validRandV = context.validRoundAndValue.get();
                proposal = validRandV.getRight();
                validRound = Optional.of(validRandV.getLeft());
                getOwner().getLogger().info(
                        "Re-Proposing the olde proposal from valid round " + validRound + " with " +
                                proposal.size() + " transactions"
                );
            }
            // BROADCASTs the proposal
            (new AcBroadcastTendermintProposal(
                    this.getEnvironment(),
                    this.getOwner(),
                    context.currentHeight,
                    context.currentRound,
                    proposal,
                    validRound)
            ).execute();
        } else {
            // schedule ''OnTimeoutPropose''
            context.triggerTendermintTimeout(
                    context.currentHeight,
                    context.currentRound,
                    TendermintPhase.PROPOSE);
        }
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcStartNewTendermintRound(
                getEnvironment(),
                getOwner(),
                this.roundToStart);
    }
}
