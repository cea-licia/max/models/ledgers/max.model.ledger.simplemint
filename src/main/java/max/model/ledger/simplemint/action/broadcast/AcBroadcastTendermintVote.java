/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplemint.action.broadcast;

import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.datatype.com.Message;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.simplemint.behavior.TendermintPhase;
import max.model.ledger.simplemint.data_types.message.TendermintVotePayload;
import max.model.ledger.simplemint.env.TendermintContext;
import max.model.ledger.simplemint.role.RTendermintValidator;
import max.model.network.stochastic_adversarial_p2p.action.message.AcEmitMessageTowardsNetwork;
import max.model.network.stochastic_adversarial_p2p.action.patterns.AcBroadcastToOthersPlayingRoleAndImmediatelyToOneself;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;


import java.util.*;


/**
 * Action executed when a Tendermint validator broadcasts a prevote or precommit to all the other Tendermint validators.
 *
 * @author Erwan Mahe
 */
public class AcBroadcastTendermintVote<T_tx, T_st extends AbstractLocalLedgerState<T_tx>, A extends StochasticPeerAgent> extends Action<A> {


    private final int height;

    private final int round;

    private final Optional<Integer> proposalHashOrNull;

    private final TendermintPhase phase;

    /**
     * Action to broadcast a Tendermint vote (PREVOTE or PRECOMMIT) to the Tendermint validators of the Tendermint environment.
     *
     * The sender is the node on which this action is executed (we retrieve its name/address to put it in the message).
     * The receivers are all the other agents that play the RTendermintValidator role in the same environment.
     *
     * Height, round and phase are parameters (the phase must not be PROPOSE otherwise an error occurs which is wanted).
     * The value of the vote is represented by an optional Integer, the empty option corresponding to the NIL vote.
     *
     * @author Erwan Mahe
     */
    public AcBroadcastTendermintVote(String drEnvironmentName,
                                     A owner,
                                     int height,
                                     int round,
                                     TendermintPhase phase,
                                     Optional<Integer> proposalHashOrNull) {
        super(drEnvironmentName, RTendermintValidator.class, owner);
        this.height = height;
        this.round = round;
        assert(!phase.equals(TendermintPhase.PROPOSE));
        this.phase = phase;
        this.proposalHashOrNull = proposalHashOrNull;
    }

    @Override
    public void execute() {
        // create payload and message
        TendermintVotePayload payload = new TendermintVotePayload(
                this.getOwner().getName(),
                this.height,
                this.round,
                this.proposalHashOrNull,
                this.phase);
        String msgType = this.phase.name();
        String msgCountKey = this.phase.name() + "_H" + this.height + "_R" + this.round;
        (new AcBroadcastToOthersPlayingRoleAndImmediatelyToOneself<TendermintVotePayload,A>(
                this.getEnvironment(),
                this.getOwner(),
                payload,
                msgType,
                msgCountKey,
                RTendermintValidator.class,
                new HashSet<>()
        )).execute();
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcBroadcastTendermintVote(
                getEnvironment(),
                getOwner(),
                this.height,
                this.round,
                this.phase,
                this.proposalHashOrNull);
    }
}
