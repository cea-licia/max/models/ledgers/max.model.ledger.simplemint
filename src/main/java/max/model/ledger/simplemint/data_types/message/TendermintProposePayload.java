/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplemint.data_types.message;

import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.Optional;


/**
 * Payload of Tendermint PROPOSE messages.
 *
 * @author Erwan Mahe
 */
public class TendermintProposePayload<T_tx> {

    public final String votingValidatorName;

    public final Integer height;

    public final Integer round;

    public final List<T_tx> proposal;

    public final Optional<Integer> validRound;

    public TendermintProposePayload(String votingValidatorName,
                                    Integer height,
                                    Integer round,
                                    List<T_tx> proposal,
                                    Optional<Integer> validRound) {
        this.votingValidatorName = votingValidatorName;
        this.height = height;
        this.round = round;
        this.proposal = proposal;
        this.validRound = validRound;
    }

    @Override
    public int hashCode() {
        int result = this.votingValidatorName.hashCode();
        result = 31 * result + Pair.of(this.height,this.round).hashCode();
        result = result*this.proposal.hashCode();
        return result;
    }

    @Override
    public String toString() {
        final var sb = new StringBuilder("{");
        sb.append("sendBy=").append(this.votingValidatorName);
        sb.append(", height=").append(this.height);
        sb.append(", round=").append(this.round);
        sb.append(", val=").append(this.proposal);
        sb.append('}');
        return sb.toString();
    }
}
