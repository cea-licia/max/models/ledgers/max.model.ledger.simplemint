/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplemint.data_types.hasher;


import java.util.List;

/**
 * A hasher that specifies how transactions and Blocks are hashed
 *
 * @author Erwan Mahe
 */
public abstract class TmAbstractTransactionsHasher<T_tx> {

    public abstract int make_transaction_hash(T_tx tx);

    public int make_block_hash(List<T_tx> block) {
        return block.hashCode();
    }

}
