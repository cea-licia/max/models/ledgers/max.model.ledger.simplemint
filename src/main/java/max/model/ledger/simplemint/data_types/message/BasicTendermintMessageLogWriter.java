/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplemint.data_types.message;


import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.network.stochastic_adversarial_p2p.msglog.AbstractMessageLogWriter;


/**
 * So that any Tendermint Node may write into a log file any Tendermint message event it observes locally i.e.,
 * the occurrences of the emission or the reception of a message on that node.
 * **/
public class BasicTendermintMessageLogWriter implements AbstractMessageLogWriter {


    @Override
    public String messageToString(Message<AgentAddress, ?> message) {
        if (message.getPayload() instanceof TendermintVotePayload) {
            TendermintVotePayload votePayload = (TendermintVotePayload) message.getPayload();
            String voteFor;
            if (votePayload.proposalHashOrNull.isPresent()) {
                voteFor = votePayload.proposalHashOrNull.get().toString();
            } else {
                voteFor = "NIL";
            }
            return String.format(
                    "%s(%s,%s,%s,%s)",
                    votePayload.votePhase,
                    votePayload.votingValidatorName,
                    votePayload.height,
                    votePayload.round,
                    voteFor);
        } else {
            TendermintProposePayload proposePayload = (TendermintProposePayload) message.getPayload();
            String validRoundAsStr;
            if (proposePayload.validRound.isPresent()) {
                validRoundAsStr = proposePayload.validRound.get().toString();
            } else {
                validRoundAsStr = "-1";
            }
            return String.format(
                    "PROPOSE(%s,%s,%s,%s,%s)",
                    proposePayload.votingValidatorName,
                    proposePayload.height,
                    proposePayload.round,
                    validRoundAsStr,
                    proposePayload.proposal.hashCode());
        }
    }

}
