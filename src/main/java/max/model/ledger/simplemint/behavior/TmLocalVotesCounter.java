/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplemint.behavior;

import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class TmLocalVotesCounter<T_tx> {


    /**
     * Keeps track of PROPOSE messages.
     * maps (height,round) to a Pair<List<T_tx>,Boolean>
     * with the proposal and whether it is valid or not
     * **/
    private final HashMap<Pair<Integer,Integer>, Pair<List<T_tx>,Boolean>> proposals;

    /**
     * Counts PREVOTE messages.
     * maps (height,round) to a HashMap<String,Integer>
     * which maps sender validator name to its vote (nil or hashcode of the proposal)
     * **/
    private final HashMap<Pair<Integer,Integer>, HashMap<String, Optional<Integer>>> prevotesCounter;


    /**
     * Counts PRECOMMIT messages.
     * maps (height,round) to a HashMap<String,Integer>
     * which maps sender validator name to its vote (nil or hashcode of the proposal)
     * **/
    private final HashMap<Pair<Integer,Integer>, HashMap<String, Optional<Integer>>> precommitCounter;


    public TmLocalVotesCounter() {
        this.proposals = new HashMap<>();
        this.prevotesCounter = new HashMap<>();
        this.precommitCounter = new HashMap<>();
    }

    public boolean alreadyHasProposalAtHeightAndRound(int height, int round) {
        return this.proposals.containsKey(Pair.of(height,round));
    }

    public void storeProposal(int height, int round, List<T_tx> proposal, boolean isValid) {
        this.proposals.put(
                Pair.of(height,round),
                Pair.of(proposal,isValid)
        );
    }

    public Pair<List<T_tx>,Boolean> getReceivedProposalAtHeightAndRound(int height, int round) {
        return this.proposals.get(Pair.of(height,round));
    }

    public boolean alreadyHasVoteFromNodeAtHeightRoundPhase(String nodeName, int height, int round, TendermintPhase phase) {
        assert(!phase.equals(TendermintPhase.PROPOSE));
        Pair<Integer,Integer> counterKey = Pair.of(height,round);
        if (phase.equals(TendermintPhase.PREVOTE)) {
            if (this.prevotesCounter.containsKey(counterKey)) {
                HashMap<String,Optional<Integer>> atHeightAndRound = this.prevotesCounter.get(counterKey);
                return atHeightAndRound.containsKey(nodeName);
            }
        } else {
            if (this.precommitCounter.containsKey(counterKey)) {
                HashMap<String,Optional<Integer>> atHeightAndRound = this.precommitCounter.get(counterKey);
                return atHeightAndRound.containsKey(nodeName);
            }
        }
        return false;
    }

    public void storeVote(int height, int round, TendermintPhase phase, String senderName, Optional<Integer> value) {
        assert(!phase.equals(TendermintPhase.PROPOSE));
        Pair<Integer,Integer> counterKey = Pair.of(height,round);
        if (phase.equals(TendermintPhase.PREVOTE)) {
            HashMap<String,Optional<Integer>> atHeightAndRound;
            if (this.prevotesCounter.containsKey(counterKey)) {
                atHeightAndRound = this.prevotesCounter.get(counterKey);
                atHeightAndRound.put(senderName,value);
            } else {
                atHeightAndRound = new HashMap<>();
            }
            this.prevotesCounter.put(counterKey,atHeightAndRound);
        } else {
            HashMap<String,Optional<Integer>> atHeightAndRound;
            if (this.precommitCounter.containsKey(counterKey)) {
                atHeightAndRound = this.precommitCounter.get(counterKey);
                atHeightAndRound.put(senderName,value);
            } else {
                atHeightAndRound = new HashMap<>();
            }
            this.precommitCounter.put(counterKey,atHeightAndRound);
        }
    }

    public int countVotesAtRoundHeightForSpecificValue(TendermintPhase phase, int height, int round, Optional<Integer> val) {
        HashMap<String, Optional<Integer>> atHeightAndRound = null;
        switch (phase) {
            case PROPOSE:
                throw new RuntimeException();
            case PREVOTE:
                atHeightAndRound = this.prevotesCounter.get(Pair.of(height,round));
                break;
            case PRECOMMIT:
                atHeightAndRound = this.precommitCounter.get(Pair.of(height,round));
                break;
        }
        // ***
        if (atHeightAndRound != null) {
            int count = 0;
            for (Optional<Integer> got : atHeightAndRound.values()) {
                if (got.equals(val)) {
                    count = count + 1;
                }
            }
            return count;
        } else {
            return 0;
        }
    }

    public int countVotesAtRoundHeightForAnyValue(TendermintPhase phase, int height, int round) {
        HashMap<String, Optional<Integer>> atHeightAndRound = null;
        switch (phase) {
            case PROPOSE:
                throw new RuntimeException();
            case PREVOTE:
                atHeightAndRound = this.prevotesCounter.get(Pair.of(height,round));
                break;
            case PRECOMMIT:
                atHeightAndRound = this.precommitCounter.get(Pair.of(height,round));
                break;
        }
        // ***
        if (atHeightAndRound != null) {
            return atHeightAndRound.size();
        } else {
            return 0;
        }
    }

}
