/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplemint.behavior;

import org.apache.commons.lang3.tuple.Pair;


import java.util.List;
import java.util.Random;

/**
 * We abstract away the mechanism for selecting the proposer for each height/round.
 * We rely on a singleton and PNRG with a seed that depends on the values of height and round.
 *
 * @author Erwan Mahe
 * **/
public class TmProposerSelector {

    // The field must be declared volatile
    private static volatile TmProposerSelector instance;

    private final List<String> listOfAllValidators;

    private TmProposerSelector(List<String> listOfAllValidators) {
        this.listOfAllValidators = listOfAllValidators;
    }

    public static TmProposerSelector getInstance() {
        // Double-checked locking (DCL) prevents race condition between multiple
        // threads that may attempt to get singleton instance at the same time,
        // creating separate instances as a result.
        //
        // The `result` variable is important here.
        //
        // https://refactoring.guru/fr/design-patterns/singleton
        // https://refactoring.guru/java-dcl-issue
        TmProposerSelector result = instance;
        if (result != null) {
            return result;
        } else {
            throw new RuntimeException("TmProposerSelector instance must be initialized");
        }
    }

    public static void setInstance(List<String> listOfAllValidators) {
        instance = new TmProposerSelector(listOfAllValidators);
    }

    public String getProposerAtHeightAndRound(int height, int round) {
        int seed = Pair.of(height,round).hashCode() + round;
        // random coin abstraction seeded on the wave number so that it is deterministic
        Random random_coin = new Random(seed);
        int index_of_leader = 0;
        // the initial values returned by Random.nextInt are similar for small seeds, you need to go further to observe notable differences
        // see https://stackoverflow.com/questions/12282628/why-are-initial-random-numbers-similar-when-using-similar-seeds
        // this is a problem that is inherent to PseudoRandomNumberGenerators
        for (int x =0; x < 100; x++) {
            index_of_leader = random_coin.nextInt(this.listOfAllValidators.size());
        }
        return this.listOfAllValidators.get(index_of_leader);
    }

}
