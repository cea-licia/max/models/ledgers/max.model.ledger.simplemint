/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplemint.env;



import java.math.BigDecimal;
import java.util.*;

import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationScheduleFactory;
import max.model.ledger.simplemint.action.timeout.AcTendermintTimeout;

import org.apache.commons.lang3.tuple.Pair;

import max.model.ledger.abstract_ledger.env.LedgerValidatorContext;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.simplemint.behavior.TendermintPhase;
import max.model.ledger.simplemint.behavior.TmLocalVotesCounter;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import org.apache.commons.lang3.tuple.Triple;


/**
 * The context of an agent that plays a RTendermintValidator role in a TendermintEnvironment.
 *
 * @author Erwan Mahe
 */
public class TendermintContext<T_tx, T_st extends AbstractLocalLedgerState<T_tx>, A extends StochasticPeerAgent> extends LedgerValidatorContext<T_tx, T_st> {


    /** Current height **/
    public Integer currentHeight;

    /** Current round **/
    public Integer currentRound;

    /** Current phase */
    public TendermintPhase currentPhase;

    /** Locked round and value **/
    public Optional<Pair<Integer,List<T_tx>>> lockedRoundAndValue;

    /** Valid round and value **/
    public Optional<Pair<Integer,List<T_tx>>> validRoundAndValue;

    /**
     * Storage for authentic messages:
     * - identified by height and round
     * - new messages are added iff their height greater or equal than currentHeight
     * - new PROPOSE messages are added if:
     *    + received from proposer for given (height,round) only once per (height,round)
     *    + or if the node itself is a proposer for (height,round) and proposes
     * - new PREVOTE and PRECOMMIT messages are added if:
     *    + not yet added for given (height,round)
     *  **/
    public TmLocalVotesCounter<T_tx> localTendermintVotesCounter;

    /**
     * the value that is used (locally, on this Tendermint validator)
     * to determine quorums on PREVOTES and PRECOMMITS
     * **/
    public int byzantineThresholdF;

    public BigDecimal timeoutDuration;

    /**
     * The current timeout action
     * **/
    private final HashSet<Triple<Integer,Integer,TendermintPhase>> timeoutsAlreadyTriggered;

    /**
     * Default constructor
     */
    public TendermintContext(
            StochasticPeerAgent owner,
            TendermintEnvironment<T_tx,T_st,A> tmEnvironment) {
        super(owner, tmEnvironment);
        this.timeoutsAlreadyTriggered = new HashSet<>();
    }

    public void initializeTendermintContext(int byzantineThresholdF, BigDecimal timeoutDuration) {
        this.byzantineThresholdF = byzantineThresholdF;
        this.timeoutDuration = timeoutDuration;
        // ***
        this.currentHeight = 0;
        this.currentRound = 0;
        this.currentPhase= null;
        this.lockedRoundAndValue = Optional.empty();
        this.validRoundAndValue = Optional.empty();
        // ***
        this.localTendermintVotesCounter = new TmLocalVotesCounter<T_tx>();
    }

    public boolean isTimeoutAlreadySet(int height, int round, TendermintPhase phase) {
        return this.timeoutsAlreadyTriggered.contains(Triple.of(height,round,phase));
    }

    public void triggerTendermintTimeout(int height, int round, TendermintPhase phase) {
        assert(!this.isTimeoutAlreadySet(height,round,phase));
        this.timeoutsAlreadyTriggered.add(Triple.of(height,round,phase));
        this.getOwner().getLogger().info("TRIGGERING timeout for height " + height + " round " + round + " phase " + phase.name());
        BigDecimal timeoutTriggerTime = this.getOwner().getSimulationTime().getCurrentTick().add(this.timeoutDuration);
        this.getOwner().schedule(new ActionActivator<>(
                ActivationScheduleFactory.createOneTime(timeoutTriggerTime),
                new AcTendermintTimeout<>(this.getEnvironmentName(), (A) this.getOwner(),phase,height,round)
        ));
    }

}

