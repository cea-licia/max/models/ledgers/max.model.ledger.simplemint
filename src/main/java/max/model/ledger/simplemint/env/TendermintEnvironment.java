/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplemint.env;

import max.core.Context;
import max.core.agent.SimulatedAgent;
import max.model.ledger.abstract_ledger.env.AbstractLedgerEnvironment;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.simplemint.role.RTendermintValidator;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;

/**
 * Tendermint environment
 *
 * @author Erwan Mahe
 */
public class TendermintEnvironment<T_tx,T_st extends AbstractLocalLedgerState<T_tx>, A extends StochasticPeerAgent> extends AbstractLedgerEnvironment<T_tx,T_st> {

    /**
     * Default constructor.
     */
    public TendermintEnvironment() {
        super();
        addAllowedRole(RTendermintValidator.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Context createContext(SimulatedAgent agent) {
        return new TendermintContext<T_tx,T_st,A>((StochasticPeerAgent) agent, this);
    }

}
