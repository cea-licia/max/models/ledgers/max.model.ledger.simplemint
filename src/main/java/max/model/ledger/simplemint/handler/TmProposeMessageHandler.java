/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplemint.handler;


import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.ledger.abstract_ledger.action.transact.AcDeliverBlockOrWaveOfTransactions;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;

import max.model.ledger.simplemint.behavior.TendermintPhase;
import max.model.ledger.simplemint.action.broadcast.AcBroadcastTendermintVote;
import max.model.ledger.simplemint.action.AcStartNewTendermintHeight;
import max.model.ledger.simplemint.data_types.hasher.TmAbstractTransactionsHasher;
import max.model.ledger.simplemint.data_types.hasher.TmHasherSingleton;
import max.model.ledger.simplemint.data_types.message.TendermintProposePayload;
import max.model.ledger.simplemint.env.TendermintContext;
import max.model.ledger.simplemint.behavior.TmProposerSelector;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.context.handler.OnReceiveMessageHandler;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Optional;


/**
 * Message handler that handles {@link TendermintPhase#PROPOSE} messages.
 *
 * @author Erwan Mahe
 */
public class TmProposeMessageHandler<T_tx, T_st extends AbstractLocalLedgerState<T_tx>, A extends StochasticPeerAgent> implements OnReceiveMessageHandler {

    @Override
    public void handle(StochasticP2PContext context, Message<AgentAddress, ?> message) {
        // ***
        TendermintContext<T_tx,T_st,A> tmContext = (TendermintContext<T_tx,T_st,A>) context;

        TendermintProposePayload<T_tx> tmPayload = (TendermintProposePayload<T_tx>) message.getPayload();

        // ***
        // VARIOUS VERIFICATIONS on the validity of the input PROPOSE message
        // it should be send by the expected proposer node at that (height,round)
        String expectedProposer = TmProposerSelector.getInstance().getProposerAtHeightAndRound(tmPayload.height,tmPayload.round);
        if (!tmPayload.votingValidatorName.equals(expectedProposer)) {
            tmContext.getOwner().getLogger().warning(
                    "UNEXPECTED message handled : PROPOSE message that is not from the expected proposer at height " + String.valueOf(tmPayload.height) +
                            " and round " + String.valueOf(tmPayload.round) + " : expected " + expectedProposer + " got " + tmPayload.votingValidatorName
            );
            return;
        }
        // we ignore messages from previous (already resolved) heights
        if (tmContext.currentHeight > tmPayload.height) {
            tmContext.getOwner().getLogger().fine(
                    "IGNORING old message from height : " + String.valueOf(tmPayload.height) +
                            " because current height is " + String.valueOf(tmContext.currentHeight)
            );
            return;
        }
        // we ignore duplicated and equivocated messages
        if (tmContext.localTendermintVotesCounter.alreadyHasProposalAtHeightAndRound(tmPayload.height, tmPayload.round)) {
            tmContext.getOwner().getLogger().fine(
                    "IGNORING PROPOSE message that was already received at height : " + String.valueOf(tmPayload.height) +
                            " and round " + String.valueOf(tmPayload.round)
            );
            return;
        }

        // ***
        // we verify whether or not the proposal is valid
        boolean isProposalValid = true;
        for (T_tx tx: tmPayload.proposal) {
            if (!tmContext.approvalPolicy.approve(tx,tmContext.ledgerValidatorState)) {
                // there is an invalid transaction in the block proposal
                // and we are in the PROPOSE phase of the relevant height and round
                if ( tmPayload.height.equals(tmContext.currentHeight) &&
                        tmPayload.round.equals(tmContext.currentRound) &&
                        tmContext.currentPhase.equals(TendermintPhase.PROPOSE)
                ) {
                    // Go to the PREVOTE phase
                    tmContext.currentPhase = TendermintPhase.PREVOTE;
                    // broadcasts a PREVOTE message on NIL
                    (new AcBroadcastTendermintVote<>(
                            tmContext.getEnvironmentName(),
                            (StochasticPeerAgent) tmContext.getOwner(),
                            tmPayload.height,
                            tmPayload.round,
                            TendermintPhase.PREVOTE,
                            Optional.empty())).execute();
                }
                isProposalValid = false;
                break;
            }
        }

        // WE STORE the received PROPOSAL
        tmContext.localTendermintVotesCounter.storeProposal(
                tmPayload.height,
                tmPayload.round,
                tmPayload.proposal,
                isProposalValid
        );

        // If the message concerns:
        //  - a height that is higher than the node's current height
        //  - or a round that is hiehger than the node's curent round
        // then we do not react to it (it will be done upon reaching the new height/round)
        if ((tmPayload.height > tmContext.currentHeight) || tmPayload.round > tmContext.currentRound) {
            return;
        }

        // If the block proposal contains transactions that are not valid
        // we ignore it for further treatment
        if (!isProposalValid) {
            return;
        }

        TmAbstractTransactionsHasher<T_tx> hasher = (TmAbstractTransactionsHasher<T_tx>) TmHasherSingleton.getInstance().get_concrete_hasher_for_TM_environment(tmContext.getEnvironmentName());
        Integer hashOfProposal = hasher.make_block_hash(tmPayload.proposal);

        // ***
        // the following hence supposes receiving a PROPOSE with a valid proposal for the current height
        // ***
        assert(isProposalValid);
        assert(tmPayload.height.equals(tmContext.currentHeight));

        if (tmPayload.round.equals(tmContext.currentRound) && tmContext.currentPhase.equals(TendermintPhase.PROPOSE)) {
            if (tmPayload.validRound.isEmpty()) {
                boolean okToPrevote;
                if (tmContext.lockedRoundAndValue.isEmpty()) {
                    okToPrevote = true;
                } else {
                    Pair<Integer,List<T_tx>> got = (Pair<Integer,List<T_tx>>) tmContext.lockedRoundAndValue.get();
                    List<T_tx> lockedValue = got.getRight();
                    okToPrevote = lockedValue.equals(tmPayload.proposal);
                }
                // ***
                Optional<Integer> prevoteHash;
                if (okToPrevote) {
                    prevoteHash = Optional.of(hashOfProposal);
                } else {
                    prevoteHash = Optional.empty();
                }
                // ***
                // Go to the PREVOTE phase
                tmContext.currentPhase = TendermintPhase.PREVOTE;
                // broadcasts a PREVOTE message
                (new AcBroadcastTendermintVote<>(
                        tmContext.getEnvironmentName(),
                        (StochasticPeerAgent) tmContext.getOwner(),
                        tmPayload.height,
                        tmPayload.round,
                        TendermintPhase.PREVOTE,
                        prevoteHash)).execute();
            } else {
                // the valid round in the input PROPOSAL is not -1
                // hence, we must check in the message storage if there are 2f+1 PREVOTE for that proposal in the corresponding round
                int prevotesCount = tmContext.localTendermintVotesCounter.countVotesAtRoundHeightForSpecificValue(
                        TendermintPhase.PREVOTE,
                        tmPayload.height,
                        tmPayload.round,
                        Optional.of(hashOfProposal));
                if (prevotesCount > 2*tmContext.byzantineThresholdF) {
                    // the node will also prevote, having had more than 2f+1 PREVOTE for the valid round of the proposal for the proposal hash
                    boolean voteForNil;
                    if (tmContext.lockedRoundAndValue.isEmpty()) {
                        voteForNil = false;
                    } else { // if the current node has locked a value then further checks are required
                        Pair<Integer,List<T_tx>> lockedRV = (Pair<Integer,List<T_tx>>) tmContext.lockedRoundAndValue.get();
                        if  (lockedRV.getLeft() <= tmPayload.validRound.get() ||
                                lockedRV.getRight().equals(tmPayload.proposal)
                        ) {
                            voteForNil = false;
                        } else {
                            voteForNil = true;
                        }
                    }
                    Optional<Integer> prevoteHash;
                    if (voteForNil) {
                        prevoteHash = Optional.empty();
                    } else {
                        prevoteHash = Optional.of(hashOfProposal);
                    }
                    // ***
                    // Go to the PREVOTE phase
                    tmContext.currentPhase = TendermintPhase.PREVOTE;
                    // broadcasts a PREVOTE message
                    (new AcBroadcastTendermintVote<>(
                            tmContext.getEnvironmentName(),
                            (StochasticPeerAgent) tmContext.getOwner(),
                            tmPayload.height,
                            tmPayload.round,
                            TendermintPhase.PREVOTE,
                            prevoteHash)).execute();
                }
            }
        }

        if (tmPayload.round.equals(tmContext.currentRound) && (!tmContext.currentPhase.equals(TendermintPhase.PROPOSE))) {
            int prevotesCount = tmContext.localTendermintVotesCounter.countVotesAtRoundHeightForSpecificValue(
                    TendermintPhase.PREVOTE,
                    tmPayload.height,
                    tmPayload.round,
                    Optional.of(hashOfProposal));
            if (prevotesCount > 2*tmContext.byzantineThresholdF) {
                // if validValue not already set, we set it
                if (tmContext.validRoundAndValue.isEmpty()) {
                    tmContext.validRoundAndValue = Optional.of(Pair.of(tmPayload.round,tmPayload.proposal));
                }
                if (tmContext.currentPhase.equals(TendermintPhase.PREVOTE)) {
                    tmContext.lockedRoundAndValue = Optional.of(Pair.of(tmPayload.round,tmPayload.proposal));
                    // ***
                    // Go to the PRECOMMIT phase
                    tmContext.currentPhase = TendermintPhase.PRECOMMIT;
                    // broadcasts a PRECOMMIT message
                    Optional<Integer> precommitHash = Optional.of(hashOfProposal);
                    (new AcBroadcastTendermintVote<>(
                            tmContext.getEnvironmentName(),
                            (StochasticPeerAgent) tmContext.getOwner(),
                            tmPayload.height,
                            tmPayload.round,
                            TendermintPhase.PRECOMMIT,
                            precommitHash)).execute();
                }
            }
        }

        if (tmPayload.height.equals(tmContext.currentHeight)) {
            // if there are more than 2f+1 precommits, consensus is reached and we go to next height
            int precommitsCount = tmContext.localTendermintVotesCounter.countVotesAtRoundHeightForSpecificValue(
                    TendermintPhase.PRECOMMIT,
                    tmPayload.height,
                    tmPayload.round,
                    Optional.of(hashOfProposal));
            if (precommitsCount > 2*tmContext.byzantineThresholdF) {
                (new AcDeliverBlockOrWaveOfTransactions(
                        tmContext.getEnvironmentName(),
                        (StochasticPeerAgent) tmContext.getOwner(),tmPayload.proposal)
                ).execute();
                (new AcStartNewTendermintHeight(
                        tmContext.getEnvironmentName(),
                        (StochasticPeerAgent) tmContext.getOwner())
                ).execute();
            }
        }

    }



}
