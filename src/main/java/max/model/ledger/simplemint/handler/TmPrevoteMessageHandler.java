/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplemint.handler;


import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;

import max.model.ledger.simplemint.action.AcStartNewTendermintRound;
import max.model.ledger.simplemint.behavior.TendermintPhase;
import max.model.ledger.simplemint.action.broadcast.AcBroadcastTendermintVote;
import max.model.ledger.simplemint.data_types.hasher.TmAbstractTransactionsHasher;
import max.model.ledger.simplemint.data_types.hasher.TmHasherSingleton;
import max.model.ledger.simplemint.data_types.message.TendermintVotePayload;
import max.model.ledger.simplemint.env.TendermintContext;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.context.handler.OnReceiveMessageHandler;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Optional;


/**
 * Message handler that handles {@link TendermintPhase#PREVOTE} messages.
 *
 * @author Erwan Mahe
 */
public class TmPrevoteMessageHandler<T_tx, T_st extends AbstractLocalLedgerState<T_tx>, A extends StochasticPeerAgent> implements OnReceiveMessageHandler {

    @Override
    public void handle(StochasticP2PContext context, Message<AgentAddress, ?> message) {
        // ***
        TendermintContext<T_tx,T_st,A> tmContext = (TendermintContext<T_tx,T_st,A>) context;

        TendermintVotePayload tmPayload = (TendermintVotePayload) message.getPayload();
        assert(tmPayload.votePhase.equals(TendermintPhase.PREVOTE));

        // ***
        // VARIOUS VERIFICATIONS on the validity of the input PREVOTE message
        // we ignore messages from previous (already resolved) heights
        if (tmContext.currentHeight > tmPayload.height) {
            tmContext.getOwner().getLogger().fine(
                    "IGNORING old message from height : " + String.valueOf(tmPayload.height) +
                            " because current height is " + String.valueOf(tmContext.currentHeight)
            );
            return;
        }
        // we ignore duplicated and equivocated messages
        if (tmContext.localTendermintVotesCounter.alreadyHasVoteFromNodeAtHeightRoundPhase(tmPayload.votingValidatorName,tmPayload.height, tmPayload.round,tmPayload.votePhase)) {
            tmContext.getOwner().getLogger().fine(
                    "IGNORING PREVOTE message that was already received at height : " + String.valueOf(tmPayload.height) +
                            " and round " + String.valueOf(tmPayload.round)
            );
            return;
        }

        // WE STORE the received PREVOTE
        tmContext.localTendermintVotesCounter.storeVote(
                tmPayload.height,
                tmPayload.round,
                tmPayload.votePhase,
                tmPayload.votingValidatorName,
                tmPayload.proposalHashOrNull
        );

        // If the message concerns a height that is higher than the node's current height
        // then we do not react to it (it will be done upon reaching the new height)
        if (tmPayload.height > tmContext.currentHeight) {
            return;
        }

        assert(tmPayload.height.equals(tmContext.currentHeight));
        // If the message concerns a round that is higher than the node's current round
        // we may advance round if enough messages on this higher round have been received
        if (tmPayload.round > tmContext.currentRound) {
            int higherRoundPrevotesCount = tmContext.localTendermintVotesCounter.countVotesAtRoundHeightForAnyValue(
                    TendermintPhase.PREVOTE,
                    tmPayload.height,
                    tmPayload.round);
            int higherRoundPrecommitsCount = tmContext.localTendermintVotesCounter.countVotesAtRoundHeightForAnyValue(
                    TendermintPhase.PRECOMMIT,
                    tmPayload.height,
                    tmPayload.round);
            int numMessagesAtHigherRound = higherRoundPrevotesCount + higherRoundPrecommitsCount;
            if (tmContext.localTendermintVotesCounter.alreadyHasProposalAtHeightAndRound(tmPayload.height,tmPayload.round)) {
                numMessagesAtHigherRound += 1;
            }
            if (numMessagesAtHigherRound > tmContext.byzantineThresholdF) {
                tmContext.getOwner().getLogger().warning(
                        "RECEIVED f+1 messages on a higher round than the current round, skipping round directly from "
                                + tmContext.currentRound + " to " + tmPayload.round
                );
                (new AcStartNewTendermintRound<>(tmContext.getEnvironmentName(),(A) tmContext.getOwner(), tmPayload.round)).execute();
                // return here because, in any case, we are now in a new round
                return;
            }
        }

        int prevotesCount = tmContext.localTendermintVotesCounter.countVotesAtRoundHeightForSpecificValue(
                TendermintPhase.PREVOTE,
                tmPayload.height,
                tmPayload.round,
                tmPayload.proposalHashOrNull);
        if (prevotesCount > 2*tmContext.byzantineThresholdF) {
            // if we received 2f+1 prevotes for NIL, we precommit NIL
            if (tmPayload.proposalHashOrNull.isEmpty() && tmContext.currentPhase.equals(TendermintPhase.PREVOTE)) {
                // Go to the PRECOMMIT phase
                tmContext.currentPhase = TendermintPhase.PRECOMMIT;
                // Broadcasts the PRECOMMIT
                (new AcBroadcastTendermintVote<>(
                        tmContext.getEnvironmentName(),
                        (A) tmContext.getOwner(),
                        tmPayload.height,
                        tmPayload.round,
                        TendermintPhase.PRECOMMIT,
                        Optional.empty())).execute();
            }

            if (tmPayload.proposalHashOrNull.isPresent() && !(tmContext.currentPhase.equals(TendermintPhase.PROPOSE))) {
                // did we receive the proposal on which there are 2f+1 PREVOTES ?
                if (tmContext.localTendermintVotesCounter.alreadyHasProposalAtHeightAndRound(tmPayload.height,tmPayload.round)) {
                    Pair<List<T_tx>,Boolean> got = tmContext.localTendermintVotesCounter.getReceivedProposalAtHeightAndRound(tmPayload.height,tmPayload.round);
                    // we verify that we have the same hash as the hash of the proposal
                    // if it is not the same maybe the proposer must have done some equivocation
                    TmAbstractTransactionsHasher<T_tx> hasher = (TmAbstractTransactionsHasher<T_tx>) TmHasherSingleton.getInstance().get_concrete_hasher_for_TM_environment(tmContext.getEnvironmentName());
                    Integer hashOfMemorizedProposal = hasher.make_block_hash(got.getLeft());
                    // ***
                    if (hashOfMemorizedProposal.equals(tmPayload.proposalHashOrNull.get())) {
                        if (got.getRight()) {
                            if (tmContext.currentPhase.equals(TendermintPhase.PREVOTE)) {
                                tmContext.lockedRoundAndValue = Optional.of(Pair.of(tmPayload.round,got.getLeft()));
                                // ***
                                // Go to the PRECOMMIT phase
                                tmContext.currentPhase = TendermintPhase.PRECOMMIT;
                                // broadcasts a PRECOMMIT message
                                (new AcBroadcastTendermintVote<>(
                                        tmContext.getEnvironmentName(),
                                        (A) tmContext.getOwner(),
                                        tmPayload.height,
                                        tmPayload.round,
                                        TendermintPhase.PRECOMMIT,
                                        tmPayload.proposalHashOrNull)).execute();
                            }
                            // if validValue not already set, we set it
                            if (tmContext.validRoundAndValue.isEmpty()) {
                                tmContext.validRoundAndValue = Optional.of(Pair.of(tmPayload.round,got.getLeft()));
                            }
                        }
                    }
                }
            }
        }

        if (!tmContext.isTimeoutAlreadySet(tmPayload.height,tmPayload.round,TendermintPhase.PREVOTE)) {
            int allPrevotesCount = tmContext.localTendermintVotesCounter.countVotesAtRoundHeightForAnyValue(
                    TendermintPhase.PREVOTE,
                    tmPayload.height,
                    tmPayload.round);
            if (allPrevotesCount > 2*tmContext.byzantineThresholdF) {
                if (
                        tmContext.currentHeight.equals(tmPayload.height) &&
                                tmContext.currentRound.equals(tmPayload.round) &&
                                tmContext.currentPhase.equals(TendermintPhase.PREVOTE)
                ) {
                    // schedule ''OnTimeoutPrevote''
                    tmContext.triggerTendermintTimeout(
                            tmContext.currentHeight,
                            tmContext.currentRound,
                            TendermintPhase.PREVOTE);
                }
            }
        }


    }



}
