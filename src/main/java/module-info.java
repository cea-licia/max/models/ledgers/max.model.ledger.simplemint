open module max.model.ledger.simplemint {
    // Exported packages
    exports max.model.ledger.simplemint.action;
    exports max.model.ledger.simplemint.behavior;
    exports max.model.ledger.simplemint.data_types.hasher;
    exports max.model.ledger.simplemint.data_types.message;
    exports max.model.ledger.simplemint.env;
    exports max.model.ledger.simplemint.handler;
    exports max.model.ledger.simplemint.role;
    exports max.model.ledger.simplemint.action.broadcast;

    // Dependencies
    requires java.logging;
    requires java.desktop;
    requires org.apache.commons.lang3;

    requires madkit;      // Automatic

    requires max.core;
    requires max.datatype.com;
    requires max.model.network.stochastic_adversarial_p2p;
    requires max.model.ledger.abstract_ledger;

    // Optional dependency (required for tests)
    requires static org.junit.jupiter.api;

}