# max.model.ledger.simplemint

Simple Tendermint model that allows adversarial attacks.

It substitutes [max.model.ledger.tendermint_v2](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.tendermint_v2)
and is based on [max.model.ledger.abstract_ledger](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.abstract_ledger) 
and [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p) rather than on
[max.model.ledger.blockchain](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.blockchain)
and [max.model.network.p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.p2p).


## Application to a mockup usecase

Although the model is generic, we apply it in the following to a specific use case in which clients compete to solve puzzles.
For any given puzzle, the first client that submits a solution and has the corresponding transaction delivered before the others wins.

We implement this usecase on top of a blockchain that uses successive instances of Tendermint to build its blocks.

Run the test in "BasicPuzzleSolvingTest" to reproduce the simulation.

We can see in the logs:

1. The nodes are set-up

<img src="./README_images/init.png">

2. The blockchain is initialized and proposers start proposing blocks (which may be empty)

<img src="./README_images/start.png">

3. The clients (mockup handled by the test environment) broadcast their solutions to the Tendermint validators with random delays

<img src="./README_images/client_broad.png">

4. As the protocol is being executed, individual nodes broadcast messages...

<img src="./README_images/send_prevote.png">

5. ...and receive messages

<img src="./README_images/forward.png">

6. Blocks are eventually delivered and the local ledger states on the nodes on which the delivery occurs are updated

<img src="./README_images/deliver_block.png">


## Verifications actions

At any moment during the simulation we can verify a number of properties using dedicated actions.
In the following we present such verifications, performed at the end of the simulation for our example with repeated puzzle games.

1. We may check the coherence of the ledger (i.e. that for any two Tendermint nodes, their local copies of the blockchain are such that one
is a prefix of the other) :

<img src="./README_images/check_coherence.png"> 


2. We may also also check the presence of duplicated delivered transactions i.e. if, in the blockchain, 
there are several occurrences of the same transaction (e.g. within the same block or in different blocks) :

<img src="./README_images/check_duplicated.png"> 


3. We may check various order-fairness and client-fairness properties related to the order of reception of the transactions, the order of delivery of the transactions
and the likelihood, for each client, to win puzzles :

<img src="./README_images/check_fairness.png"> 


4. We may count the number of messages that have transited in the network layer (i.e. in [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p)) :

<img src="./README_images/check_msg_count.png"> 


## How to rerun the simulation

Run the test in "BasicPuzzleSolvingTest.java" to rerun the example simulation on your own machine.

## Logs of input and output events

Via the API from  [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p),
it is also possible to generate log files that only relate to the atomic message input and output events
that occur on each node.

For instance, below, for a simulation with 10 nodes, we have 10 log files, each 
corresponding to a sequence of input and output event observed locally on one of
the nodes.

<img src="./README_images/logs.png"> 

## Implementation of attacks

Because this model is built on top of [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p),
we can use its adversarial model.

In addition, the behavior of all Tendermint nodes being defined purely locally (no global oracle in contrast to [max.model.ledger.tendermint_v2](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.tendermint_v2))
, an adversary can attack individual nodes to modify their behaviors.
This can be done e.g. via:
- "AcSetLocalApprovalPolicy" from [max.model.ledger.abstract_ledger](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.abstract_ledger)
- "AcAddCommunicationCensor" from [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p)
- "AcAddCommunicationDelay" from [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p)
- "AcAddCommunicationRedirect" from [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p)
- "AcConfigMessageHandler" from [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p) which can be used to change the manner with
which the node will react to PROPOSE, PREVOTE and PRECOMMIT messages


## Features not supported

This model is a simple Tendermint model. It does not implement:
- changing committee composition (the committee always include all the TendermintValidator nodes in the environment)
- the variation of the duration of timeout phases depending on the reception of heartbeat messages


# Licence

__max.model.ledger.simplemint__ is released under
the [Eclipse Public Licence 2.0 (EPL 2.0)](https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html)













